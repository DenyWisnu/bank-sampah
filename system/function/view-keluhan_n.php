<?php
// Include your database connection and any necessary functions
error_reporting(E_ALL | E_STRICT);
require_once("../system/config/koneksi.php");

// Fetch data from the 'keluhan' table
$query = mysqli_query($conn, "SELECT * FROM keluhan ORDER BY tanggal_keluhan DESC");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Keluhan</title>
    <link rel="stylesheet" type="text/css" href="../datatables/css/jquery.dataTables.css">
    <style>
        label {
            font-family: Montserrat;
            font-size: 18px;
            display: block;
            color: #262626;
        }

        body {
            font-family: Montserrat;
            font-size: 16px;
        }

        h2 {
            font-size: 30px;
            color: #262626;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        th {
            background-color: #f2f2f2;
        }

        .action-buttons {
            display: flex;
            justify-content: center;
        }

        .action-buttons a {
            margin: 0 5px;
            text-decoration: none;
        }

        .action-buttons button {
            padding: 5px 10px;
            cursor: pointer;
        }

        .action-buttons .tanggapi-button {
            background-color: #3498db;
            color: #fff;
        }
    </style>
</head>
<body>
    <h2>Data Keluhan</h2>
    <br>

    <table id="example" class="display" cellspacing="0" border="0">
        <thead>
            <tr>
                <th>No</th>
                <th>NIN</th>
                <th>Tanggal Keluhan</th>
                <th>Isi Keluhan</th>
                <th>Status</th>
                <th>Isi Tanggapan</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 0;
            while ($row = mysqli_fetch_assoc($query)) {
                $no++;
                $id_keluhan = $row['id_keluhan'];
                $tanggapan_query = mysqli_query($conn, "SELECT isi_tanggapan FROM tanggapan WHERE id_keluhan='$id_keluhan'");
                $tanggapan_data = mysqli_fetch_assoc($tanggapan_query);
            ?>
            <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo htmlspecialchars($row['nin']); ?></td>
                <td><?php echo htmlspecialchars($row['tanggal_keluhan']); ?></td>
                <td><?php echo htmlspecialchars($row['isi_keluhan']); ?></td>
                <td><?php echo htmlspecialchars($row['status']); ?></td>
                <td><?php echo isset($tanggapan_data['isi_tanggapan']) ? htmlspecialchars($tanggapan_data['isi_tanggapan']) : ''; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>

    <a href="nasabah.php?page=tambah-keluhan">    
        <button><i class="fa fa-plus" aria-hidden="true"></i>Tambah</button>
    </a>

    <!-- Add any additional HTML, styles, or scripts -->

    <script type="text/javascript" src="../datatables/js/jquery.min.js"></script>
    <script type="text/javascript" src="../datatables/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
</body>
</html>

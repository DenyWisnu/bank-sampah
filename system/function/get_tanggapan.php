<?php
// get_tanggapan.php

// Include your database connection
error_reporting(E_ALL | E_STRICT);
require_once("../system/config/koneksi.php");

// Check if id_keluhan is set
if (isset($_GET['id_keluhan'])) {
    $id_keluhan = $_GET['id_keluhan'];

    // Fetch tanggapan data from the 'tanggapan' table based on id_keluhan
    $tanggapan_query = mysqli_query($conn, "SELECT * FROM tanggapan WHERE id_keluhan='$id_keluhan'");
    $tanggapan_data = mysqli_fetch_assoc($tanggapan_query);

    // Display the tanggapan content
    echo htmlspecialchars($tanggapan_data['isi_tanggapan']);
} else {
    echo "Invalid Request";
}
?>

<?php
if (empty($_SESSION['nia'])) {
    header('location: login.php');
    exit();
}

error_reporting(E_ALL | E_STRICT);
include_once("../system/config/koneksi.php");

// Mengambil NIA Admin dari tabel admin
$nia = $_SESSION['nia'];
$admin_query = mysqli_query($conn, "SELECT * FROM admin WHERE nia='$nia'");

if ($admin_query) {
    $admin_data = mysqli_fetch_array($admin_query);

    // Memastikan bahwa 'nia' ada sebelum menyimpan ke sesi
    if (isset($admin_data['nia'])) {
        $_SESSION['nia'] = $admin_data['nia']; // menyimpan NIA Admin ke dalam sesi
    } else {
        echo "Kolom 'nia' tidak ditemukan dalam tabel 'admin'.";
        exit();
    }
} else {
    echo "Error: " . mysqli_error($conn);
    exit();
}

// Check if the form is submitted
if (isset($_POST['submit'])) {
    $id_keluhan = $_POST['id_keluhan'];
    $isi_tanggapan = $_POST['isi_tanggapan'];
    $status = $_POST['status'];

    // Update status in the 'keluhan' table
    $query = mysqli_query($conn, "UPDATE keluhan SET status='$status' WHERE id_keluhan='$id_keluhan'");

    if ($query) {
        // Insert response into the 'tanggapan' table
        $query_tanggapan = mysqli_query($conn, "INSERT INTO tanggapan (id_keluhan, tanggal_tanggapan, isi_tanggapan, nia) VALUES ('$id_keluhan', NOW(), '$isi_tanggapan', '$nia')");

        if ($query_tanggapan) {
            echo "<script>alert('Tanggapan berhasil ditambahkan!');</script>";
        } else {
            echo "<script>alert('Gagal menambahkan tanggapan!');</script>";
            echo "Error: " . mysqli_error($conn);  // Add this line to display the specific MySQL error
        }
    } else {
        echo "<script>alert('Gagal mengupdate status keluhan!');</script>";
        echo "Error: " . mysqli_error($conn);  // Add this line to display the specific MySQL error
    }
}

// Retrieve keluhan data based on ID
if (isset($_GET['id_keluhan'])) {
    $id_keluhan = $_GET['id_keluhan'];
    $keluhan_query = mysqli_query($conn, "SELECT * FROM keluhan WHERE id_keluhan='$id_keluhan'");
    $keluhan_data = mysqli_fetch_assoc($keluhan_query);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tanggapi Keluhan</title>
    <link rel="stylesheet" type="text/css" href="../datatables/css/jquery.dataTables.css">
    <style>
        label {
            font-family: Montserrat;
            font-size: 18px;
            display: block;
            color: #262626;
        }

        body {
            font-family: Montserrat;
            font-size: 16px;
        }

        h2 {
            font-size: 30px;
            color: #262626;
        }

        form {
            width: 50%;
            margin: 20px auto;
        }

        input[type=hidden], textarea, select {
            width: 100%;
            padding: 8px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 10px 15px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <h2>Tanggapi Keluhan</h2>

    <form action="" method="post">
        <label for="id_keluhan">ID Keluhan:</label>
        <input type="text" id="id_keluhan" name="id_keluhan" value="<?php echo htmlspecialchars($keluhan_data['id_keluhan']); ?>" readonly>

        <label for="isi_keluhan">Isi Keluhan:</label>
        <textarea id="isi_keluhan" name="isi_keluhan" rows="4" readonly><?php echo htmlspecialchars($keluhan_data['isi_keluhan']); ?></textarea>

        <label for="isi_tanggapan">Tanggapan:</label>
        <textarea id="isi_tanggapan" name="isi_tanggapan" rows="4" required></textarea>

        <label for="status">Status:</label>
        <select id="status" name="status" required>
            <option value="Processed">Processed</option>
            <option value="Resolved">Resolved</option>
        </select>

        <input type="submit" name="submit" value="Submit">
    </form>
    <!-- Add any additional HTML, styles, or scripts -->
</body>
</html>

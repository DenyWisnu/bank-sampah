<?php
// Include your database connection and any necessary functions
error_reporting(E_ALL | E_STRICT);
require_once("../system/config/koneksi.php");

// Check if the user is logged in
if (!isset($_SESSION['nin'])) {
    // Redirect to the login page or handle authentication as needed
    header("Location: login.php");
    exit();
}

// NIN of the logged-in user
$nin = $_SESSION['nin'];

// Check if the form is submitted
if (isset($_POST['submit'])) {
    $isi_keluhan = $_POST['isi_keluhan'];

    // Insert complaint into the 'keluhan' table
    $query = mysqli_query($conn, "INSERT INTO keluhan (nin, tanggal_keluhan, isi_keluhan, status) VALUES ('$nin', NOW(), '$isi_keluhan', 'Pending')");

    if ($query) {
        echo "
        <script>
            alert('Keluhan berhasil ditambahkan!');
        </script>
        ";
    } else {
        echo "
        <script>
            alert('Gagal menambahkan keluhan!');
        </script>
        ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tambah Keluhan</title>
    <link rel="stylesheet" type="text/css" href="../datatables/css/jquery.dataTables.css">
    <style>
        label {
            font-family: Montserrat;
            font-size: 18px;
            display: block;
            color: #262626;
        }

        body {
            font-family: Montserrat;
            font-size: 16px;
        }

        h2 {
            font-size: 30px;
            color: #262626;
        }

        form {
            width: 50%;
            margin: 20px auto;
        }

        input[type=text], textarea {
            width: 100%;
            padding: 8px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 10px 15px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <h2>Tambah Keluhan</h2>

    <form action="" method="post">
        <label for="nin">NIN (Nasabah Identification Number):</label>
        <input type="text" id="nin" name="nin" value="<?php echo htmlspecialchars($nin); ?>" readonly>

        <label for="isi_keluhan">Isi Keluhan:</label>
        <textarea id="isi_keluhan" name="isi_keluhan" rows="4" required></textarea>

        <input type="submit" name="submit" value="Submit">
    </form>

    <!-- Add any additional HTML, styles, or scripts -->

</body>
</html>
